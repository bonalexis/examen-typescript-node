import { readFileSync } from "fs";

type Person = {
  age: number;
  height: number;
};

function getStatistics(): { meanAge: number; meanHeight: number } {
  const persons: unknown = JSON.parse(
    readFileSync("./persons.json").toString()
  );

  if (!Array.isArray(persons)) {
    throw new Error("Le type de persons est censé être un tableau");
  }

  if (!persons.length) {
    throw new Error("Au moins une personne doit être contenue dans le tableau");
  }

  if (!persons[0].age || !persons[0].height) {
    throw new Error("Une personne doit avoir un age et une taille");
  }

  if (
    Number.isNaN(Number(persons[0].age)) ||
    Number.isNaN(Number(persons[0].height))
  ) {
    throw new Error(
      "Une personne doit avoir un age valide et une taille valide de type `number`"
    );
  }

  const validPersons = persons as Person[];

  const sumAge = validPersons
    .map(({ age }) => age)
    .reduce((prev, current) => prev + current);

  const sumHeight = validPersons
    .map(({ height }) => height)
    .reduce((prev, current) => prev + current);

  return {
    meanAge: sumAge / validPersons.length,
    meanHeight: sumHeight / validPersons.length,
  };
}

function displayResult() {
  console.log(getStatistics());
}
displayResult();
